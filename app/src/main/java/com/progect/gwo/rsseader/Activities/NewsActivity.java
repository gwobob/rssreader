package com.progect.gwo.rsseader.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.progect.gwo.rsseader.R;

final public class NewsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        TextView news = findViewById(R.id.news_text);
        news.setText(Html.fromHtml("<b>404</b><br>not found"));
    }
}
