package com.progect.gwo.rsseader.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.progect.gwo.rsseader.R;

final public class MainActivity extends AppCompatActivity {

    public Intent newsIntent;
    final String wordToAdd = "Channel";
    int chanelCounter = 0;
    ArrayAdapter<String> myAdapter;
    SharedPreferences destroyAndReturnChannelsCount;
    SharedPreferences.Editor editor;
    final String CHANNEL_COUNT_PREFERENCE_KEY = "counter";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView newsList = findViewById(R.id.main_list_view);
        myAdapter = new ArrayAdapter<>(this, R.layout.list_item);
        newsList.setAdapter(myAdapter);

        destroyAndReturnChannelsCount = getPreferences(Context.MODE_PRIVATE);
        ReturningChannels();
    }

    public void ClickForNews(final View view){
        newsIntent = new Intent(this,NewsActivity.class);
        startActivity(newsIntent);
    }

    public void ClickOnToAddChannels(final View view){
        myAdapter.add(wordToAdd + " " + chanelCounter++);
    }

    void ChannelsSaveMethod(){
        editor.putInt(CHANNEL_COUNT_PREFERENCE_KEY,chanelCounter);
        editor.apply();
    }

    void ReturningChannels(){
        try {
            chanelCounter = destroyAndReturnChannelsCount.getInt(CHANNEL_COUNT_PREFERENCE_KEY,0);
            editor = destroyAndReturnChannelsCount.edit();
            editor.clear();
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(int i = 0; i < chanelCounter; ++i)
            myAdapter.add(wordToAdd + " " + i);
    }

    @Override
    protected void onStop(){
        super.onStop();
        ChannelsSaveMethod();    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        ChannelsSaveMethod();
    }

}
